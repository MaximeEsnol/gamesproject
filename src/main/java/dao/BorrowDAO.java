/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import beans.Borrow;
import beans.Borrower;
import beans.Game;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.ConnectionManager;

/**
 *
 * @author MEOBL79
 */
public class BorrowDAO extends DAO {

    private static final String SQL_GET_ID = "SELECT * FROM borrow b LEFT JOIN game g ON g.id = b.game_id LEFT JOIN borrower br ON br.id = b.borrower_id WHERE b.id = ?";
    private static final String SQL_GET_ALL = "SELECT * FROM borrow b LEFT JOIN game g ON g.id = b.game_id LEFT JOIN borrower br ON br.id = b.borrower_id ORDER BY g.game_name ASC";
    private static final String SQL_GET_BY_BORROWER = "SELECT * FROM borrow b LEFT JOIN game g ON g.id = b.game_id LEFT JOIN borrower br ON br.id = b.borrower_id WHERE br.id = ?";

    public BorrowDAO() {
    }

    public Borrow getById(int id) throws SQLException {
        this.makeConnection();
        Borrow b = null;
        ps = con.prepareStatement(SQL_GET_ID);
        ps.setInt(1, id);
        rs = ps.executeQuery();

        if (rs.next()) {
            b = (Borrow) createObject(rs);
        }

        ConnectionManager.closeAll(rs, ps);
        return b;
    }

    public List<Borrow> getAll() throws SQLException {
        this.makeConnection();
        List<Borrow> borrows = new ArrayList<>();

        ps = con.prepareStatement(SQL_GET_ALL);
        rs = ps.executeQuery();
        while (rs.next()) {
            borrows.add((Borrow) createObject(rs));
        }

        ConnectionManager.closeAll(rs, ps);
        return borrows;
    }
    
    public List<Borrow> getByBorrower(int id) throws SQLException{
        this.makeConnection();
        List<Borrow> borrows = new ArrayList<>();
        
        ps = con.prepareStatement(SQL_GET_BY_BORROWER);
        ps.setInt(1, id);
        rs = ps.executeQuery();
        
        while(rs.next()){
            borrows.add((Borrow)createObject(rs));
        }
        
        return borrows;
    }

    @Override
    Object createObject(ResultSet rs) throws SQLException {
        Borrow b = new Borrow();
        b.setId(rs.getInt("b.id"));
        b.setBorrowDate(rs.getDate("b.borrow_date"));
        b.setReturnDate(rs.getDate("b.return_date"));

        Game g = new Game();
        g.setpK(rs.getInt("g.id"));
        g.setGameName(rs.getString("g.game_name"));

        Borrower br = new Borrower();
        br.setpK(rs.getInt("br.id"));
        br.setBorrowerName(rs.getString("br.borrower_name"));

        b.setGame(g);
        b.setBorrower(br);

        return b;
    }

}
