package dao;

import beans.Category;
import utils.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CategoryDAO extends DAO {

    public CategoryDAO() { }

    public Category findCategoryByID(int id) throws SQLException{
        Category category;
        makeConnection();
        ps = con.prepareStatement("SELECT category.id,category.category_name from category category where id = ?");
        ps.setInt(1,id);
        rs = ps.executeQuery();
        if (rs.next()) {
            category = (Category)createObject(rs);
        } else {
            throw new SQLException("no records were found.");
        }
        rs.close();
        ps.close();
        ConnectionManager.closeAll(rs, ps);
        return category;
    }
    
    @Override
    Object createObject(ResultSet rs) throws SQLException {
        return new Category(rs.getInt("category.id"),rs.getString("category.category_name"));
    }
}
