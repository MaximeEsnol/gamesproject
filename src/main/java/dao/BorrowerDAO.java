package dao;

import beans.Borrower;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import utils.ConnectionManager;

public class BorrowerDAO extends DAO{
    
    public BorrowerDAO(){}
    
    public Borrower getById(int id) throws SQLException{
        Borrower b = null;
        makeConnection();
        ps = con.prepareStatement("SELECT * FROM borrower WHERE id = ?");
        ps.setInt(1, id);
        rs = ps.executeQuery();
        if(rs.next()){
            b = (Borrower) createObject(rs);
        }
        ConnectionManager.closeAll(rs, ps);
        return b;
    }
    
    @Override
    Object createObject(ResultSet rs) throws SQLException {
        Borrower b = new Borrower();
        b.setpK(rs.getInt("id"));
        b.setBorrowerName(rs.getString("borrower_name"));
        b.setEmail(rs.getString("email"));
        b.setCity(rs.getString("city"));
        b.setPostcode(rs.getInt("postcode"));
        b.setStreet(rs.getString("street"));
        b.setHouseNumber(rs.getString("house_number"));
        b.setBusNumber(rs.getString("bus_number"));
        b.setTelephone(rs.getString("telephone"));
        return b;
    }
    
}
