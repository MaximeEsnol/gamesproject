package dao;

import beans.Category;
import beans.Difficulty;
import beans.Game;
import utils.ConnectionManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GameDAO extends DAO{
    
    public GameDAO(){}
    
    public List<Game> findByName(String name) throws SQLException{
        makeConnection();
        List<Game> games = new ArrayList<>();
        ps = con.prepareStatement("SELECT * FROM game game "
                + "  LEFT JOIN category category ON game.category_id = category.id "
                + "  LEFT JOIN difficulty difficulty ON game.difficulty_id = difficulty.id "
                + "  WHERE LOWER(game_name) LIKE LOWER(?) ORDER BY game.game_name ASC");
        
        ps.setString(1, "%" + name + "%");
        rs = ps.executeQuery();
        
        while(rs.next()){
            games.add((Game)createObject(rs));
        }
        ConnectionManager.closeAll(rs, ps);
        return games;
    }
    
    public Game findGameByID(int id) throws SQLException {
        makeConnection();
        Game game = new Game();
        ps = con.prepareStatement("select game.id," +
                                                                            "game.game_name," +
                                                                            "game.editor," +
                                                                            "game.author," +
                                                                            "game.year_edition," +
                                                                            "game.age," +
                                                                            "game.min_players," +
                                                                            "game.max_players," +
                                                                            "difficulty.id," +
                                                                            "difficulty.difficulty_name," +
                                                                            "category.id," +
                                                                            "category.category_name," +
                                                                            "game.play_duration," +
                                                                            "game.price," +
                                                                            "game.image " +
                                                                       "from game game " +
                                                                 "inner join category category " +
                                                                         "on game.category_id = category.id " +
                                                                 "inner join difficulty difficulty " +
                                                                         "on game.difficulty_id = difficulty.id" +
                                                                     " where game.id = ?;");
        ps.setInt(1, id);
        rs = ps.executeQuery();
        if (rs.next()) {
            game = (Game)createObject(rs);
        }
        ConnectionManager.closeAll(rs, ps);
        return game;
    }
    

    @Override
    Object createObject(ResultSet rs) throws SQLException {
        Difficulty d = new Difficulty(
                        rs.getInt("difficulty.id"),
                        rs.getString("difficulty.difficulty_name")
                );
        Category c = new Category(
                        rs.getInt("category.id"),
                        rs.getString("category.category_name")
                );
        return new Game(rs.getInt("game.id"),
                rs.getString("game.game_name"),
                rs.getString("game.editor"),
                rs.getString("game.author"),
                rs.getInt("game.year_edition"),
                rs.getString("game.age"),
                rs.getInt("game.min_players"),
                rs.getInt("game.max_players"),
                d,
                c,
                rs.getString("game.play_duration"),
                rs.getDouble("game.price"),
                rs.getString("game.image")
        );
    }
}
