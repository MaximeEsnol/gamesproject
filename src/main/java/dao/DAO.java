package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.logging.Level;
import java.util.logging.Logger;
import utils.ConnectionManager;

public abstract class DAO {
	
	protected Connection con;
	protected PreparedStatement ps;
	protected ResultSet rs;
	protected String[] SQL_ORDER_BYS;
	
	public DAO() {}
	
	abstract Object createObject(ResultSet rs) throws SQLException;
	
	public Connection makeConnection() {
            try {
                return con = ConnectionManager.create();
            } catch (SQLException ex) {
                Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
	}
}

