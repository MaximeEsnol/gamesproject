/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.sql.Date;
import java.util.Objects;

/**
 *
 * @author MEOBL79
 */
public class Borrow {
    private int id;
    private Game game;
    private Borrower borrower;
    private Date borrowDate;
    private Date returnDate;

    public Borrow() {
    }

    public Borrow(int id, Game game, Borrower borrower, Date borrow_date, Date return_date) {
        this.id = id;
        this.game = game;
        this.borrower = borrower;
        this.borrowDate = borrow_date;
        this.returnDate = return_date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Borrower getBorrower() {
        return borrower;
    }

    public void setBorrower(Borrower borrower) {
        this.borrower = borrower;
    }

    public Date getBorrowDate() {
        return borrowDate;
    }

    public void setBorrowDate(Date borrowDate) {
        this.borrowDate = borrowDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date return_date) {
        this.returnDate = return_date;
    }

    @Override
    public String toString() {
        return "Borrow{" + "id=" + id + ", game=" + game + ", borrower=" + borrower + ", borrow_date=" + borrowDate + ", return_date=" + returnDate + '}';
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Borrow other = (Borrow) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.game, other.game)) {
            return false;
        }
        if (!Objects.equals(this.borrower, other.borrower)) {
            return false;
        }
        if (!Objects.equals(this.borrowDate, other.borrowDate)) {
            return false;
        }
        if (!Objects.equals(this.returnDate, other.returnDate)) {
            return false;
        }
        return true;
    }
    
    
}
