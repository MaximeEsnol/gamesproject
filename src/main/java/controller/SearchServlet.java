package controller;

import beans.Borrow;
import beans.Borrower;
import beans.Category;
import beans.Game;
import dao.BorrowDAO;
import dao.BorrowerDAO;
import dao.CategoryDAO;
import dao.GameDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utils.ConnectionManager;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author MEOBL79
 */
@WebServlet(name = "SearchServlet", urlPatterns = "/search")
public class SearchServlet extends HttpServlet {
    
    BorrowerDAO borrowerDAO;
    BorrowDAO borrowDAO;
    CategoryDAO categoryDAO;
    GameDAO gameDAO;
    PrintWriter out;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        categoryDAO = new CategoryDAO();
        gameDAO = new GameDAO();
        borrowerDAO = new BorrowerDAO();
        borrowDAO = new BorrowDAO();
        out = resp.getWriter();
        checkForAction(req, resp);
    }

    private void checkForAction(HttpServletRequest req, HttpServletResponse resp) {
        if (req.getParameter("searchButton") != null) {
            doSearchGame(req, resp);
        } else if (req.getParameter("id") != null) {
            doShowSort(req, resp);
        } else if (req.getParameter("game") != null) {
            doShowGame(req, resp);
        } else if(req.getParameter("borrower") != null){
            doShowBorrower(req, resp);
        } else if(req.getParameter("allgames") != null){
            doShowAllGames(req, resp);
        } else if (req.getParameter("details") != null){
            doShowGameDetails(req, resp);
        } else if (req.getParameter("borrows") != null){
            doShowBorrows(req, resp);
        }
    }

    private void doShowGame(HttpServletRequest req, HttpServletResponse resp) {
        if(req.getParameter("game") != null){
            try {
                int id = Integer.parseInt(req.getParameter("game"));
                Game game = gameDAO.findGameByID(id);
                req.setAttribute("game", game);
                this.getServletContext().getRequestDispatcher("/WEB-INF/spel.jsp").forward(req, resp);
            } catch (SQLException ex) {
                Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ServletException ex) {
                Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void doShowSort(HttpServletRequest req, HttpServletResponse resp) {
        if (req.getParameter("id") != null) {
            try {
                int id = Integer.parseInt(req.getParameter("id"));
                Category category = categoryDAO.findCategoryByID(id);
                req.setAttribute("category", category);
                this.getServletContext().getRequestDispatcher("/WEB-INF/soort.jsp").forward(req, resp);
            } catch (SQLException ex) {
                Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ServletException ex) {
                Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void doSearchGame(HttpServletRequest req, HttpServletResponse resp) {
        if (req.getParameter("search") != null) {
            try {
                List<Game> games = gameDAO.findByName(req.getParameter("search"));
                req.setAttribute("games", games);
                this.getServletContext().getRequestDispatcher("/WEB-INF/search_results.jsp").forward(req, resp);
            } catch (SQLException ex) {
                Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ServletException ex) {
                Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void doShowBorrower(HttpServletRequest req, HttpServletResponse resp) {
        if(req.getParameter("borrower") != null){
            try {
                int id = Integer.parseInt(req.getParameter("borrower"));
                Borrower borrower = borrowerDAO.getById(id);
                req.setAttribute("borrower", borrower);
                this.getServletContext().getRequestDispatcher("/WEB-INF/borrower.jsp").forward(req, resp);
            } catch (ServletException ex) {
                Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
                    
        }
    }

    private void doShowAllGames(HttpServletRequest req, HttpServletResponse resp) {
       if(req.getParameter("allgames") != null){
           try {
               List<Game> games = gameDAO.findByName("");
               req.setAttribute("games", games);
               this.getServletContext().getRequestDispatcher("/WEB-INF/allgames.jsp").forward(req, resp);
           } catch (SQLException ex) {
               Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
           } catch (ServletException ex) {
               Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
           } catch (IOException ex) {
               Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
           }
       }
    }
    
    private void doShowGameDetails(HttpServletRequest req, HttpServletResponse resp) {
        if(req.getParameter("details") != null){
            try {
                
                int id = Integer.parseInt(req.getParameter("details"));
                Game game = gameDAO.findGameByID(id);
                req.setAttribute("game", game);
                this.getServletContext().getRequestDispatcher("/WEB-INF/gamedetails.jsp").forward(req, resp);
            } catch (SQLException ex) {
                Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ServletException ex) {
                Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void doShowBorrows(HttpServletRequest req, HttpServletResponse resp) {
        if(req.getParameter("borrows") != null){
            try {
                List<Borrow> borrows = borrowDAO.getAll();
                List<Borrower> borrowers = new ArrayList<>();
                HashMap<String, Object> data = new HashMap<>();
                
                for(Borrow b : borrows){
                    borrowers.add(b.getBorrower());
                }     
                
                data.put("borrowers", borrowers);
                
                if(req.getParameter("filter") != null){
                    int borrowerId = Integer.parseInt(req.getParameter("filter"));
                    borrows = borrowDAO.getByBorrower(borrowerId);
                } 
                
                data.put("borrows", borrows);
                
                req.setAttribute("data", data);
                this.getServletContext().getRequestDispatcher("/WEB-INF/borrows.jsp").forward(req, resp);
            } catch (SQLException ex) {
                Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ServletException ex) {
                Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
