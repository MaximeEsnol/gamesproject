
package controller;

import beans.Game;
import dao.BorrowDAO;
import dao.BorrowerDAO;
import dao.CategoryDAO;
import dao.GameDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name="AdvancedSearchServlet", urlPatterns="/advanced")
public class AdvancedSearchServlet extends HttpServlet {
    
    BorrowerDAO borrowerDAO;
    BorrowDAO borrowDAO;
    GameDAO gameDAO;
    CategoryDAO categoryDAO;
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {       
        chooseOption(req, resp); 
    }

    private void chooseOption(HttpServletRequest req, HttpServletResponse resp) {
        String param;
        
        if((param = req.getParameter("moeilijkheid")) != null){
            try {
                List<Game> games = gameDAO.findByName("");
                List<Game> filteredGames = new ArrayList<>();
                
                int difficulty = Integer.parseInt(req.getParameter("moeilijkheid"));
                
                for(Game game : games){
                    if(game.getDifficulty().getpK() >= difficulty){
                        filteredGames.add(game);
                    }
                }
                
                req.setAttribute("games", games);
                forward("/WEB-INF/advanced_difficulty.jsp", req, resp);
            } catch (SQLException ex) {
                Logger.getLogger(AdvancedSearchServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            forward("/WEB-INF/advanced_search.jsp", req, resp);
        }
    }
    
    private void forward(String url, HttpServletRequest req, HttpServletResponse resp){
        try {
            this.getServletContext().getRequestDispatcher(url).forward(req, resp);
        } catch (ServletException ex) {
            Logger.getLogger(AdvancedSearchServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AdvancedSearchServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
    
}
