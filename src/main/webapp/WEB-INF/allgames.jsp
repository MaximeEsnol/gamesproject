<%-- 
    Document   : allgames
    Created on : Feb 8, 2020, 12:11:37 PM
    Author     : MEOBL79
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>All games</title>
    </head>
    <body>
        <h1>Showing: all games</h1>
        <c:forEach items="${games}" var="game">
            <p>
                <b>Naam: </b> <a href="search?details=<c:out value='${game.getpK()}'/>"><c:out value="${game.gameName}"/> </a>
                <b>Uitgever: </b> <c:out value="${game.editor}"/> 
                <b>Soort: </b> <c:out value="${game.category.categoryName}"/> 
                <b>Prijs: </b> &euro;<c:out value="${game.price}"/> 
            </p>  
            <hr/>
        </c:forEach>
    </body>
</html>
