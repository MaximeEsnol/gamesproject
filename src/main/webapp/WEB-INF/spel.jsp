<%-- 
    Document   : spel
    Created on : Feb 8, 2020, 11:34:23 AM
    Author     : MEOBL79
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Spel</title>
    </head>
    <body>
        <h1><c:out value="${game.gameName}"/></h1>
        <img src="files/images/<c:out value='${game.image}'/>"/>
        <p><c:out value="${game.toString()}"/></p>
    </body>
</html>
