<%-- 
    Document   : search_results.jsp
    Created on : Feb 5, 2020, 1:17:50 PM
    Author     : MEOBL79
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><c:out value="${param['search']}"></c:out> - Search results</title>
    </head>
    <body>
    <c:forEach items="${games}" var="game">
        <p>
            <b>Name</b> ${game.gameName}
        </p>
        <p>
            <b>Editor</b> ${game.editor}
        </p>
        <hr/>
    </c:forEach>
    </body>
</html>
