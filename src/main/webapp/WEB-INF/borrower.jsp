<%-- 
    Document   : borrower
    Created on : Feb 8, 2020, 12:02:07 PM
    Author     : MEOBL79
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Borrower</title>
    </head>
    <body>
        <h1><c:out value="${borrower.borrowerName}"/></h1>
        <p><c:out value="${borrower.toString()}"/></p>
    </body>
</html>
