<%-- 
    Document   : soort
    Created on : Feb 8, 2020, 10:52:41 AM
    Author     : MEOBL79
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Spelsoorten</title>
    </head>
    <body>
        <a href="index.jsp">Terug naar startpagina</a>
        <p>Categorie met id ${param["id"]}</p>
        <p>
            <b>
                ${category.categoryName}
            </b>
        </p>
    </body>
</html>
