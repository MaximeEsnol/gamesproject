<%-- 
    Document   : advanced_search
    Created on : Feb 11, 2020, 1:57:10 PM
    Author     : MEOBL79
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Advanced search</title>
    </head>
    <body>
        <h1>Advanced search</h1>
        
        <form method="get" action="advanced">
            <select id="moeilijkheid" name="moeilijkheid">
                <option select disable>
                    --- Kies een moeilijkheidsgraad ---
                </option>
                <option value="1">Very easy</option>
                <option value="2">Easy</option>
                <option value="3">Average</option>
                <option value="4">Difficult</option>
                <option value="5">Very difficult</option>s
            </select>
            
            <button type="submit">Zoek!</button>
        </form>
    </body>
</html>
