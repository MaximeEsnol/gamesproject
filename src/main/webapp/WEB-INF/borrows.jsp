<%-- 
    Document   : borrows
    Created on : Feb 10, 2020, 1:07:18 PM
    Author     : MEOBL79
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Uitleningen</title>
    </head>
    <body>
        <h1>Alle uitleningen</h1>       
        
        <select id="filter" name="filter">
            <option selected disabled>--- Filter op verhuurder ---</option>
            <option value="-1">Alle</option>
            <c:forEach items="${data.borrowers}" var="select_borrower">
                <option value="${select_borrower.getpK()}"><c:out value="${select_borrower.borrowerName}"/></option>
            </c:forEach>
        </select>
        
        <c:forEach items="${data.borrows}" var="borrow">
            <p>
                <b>Spel: </b><c:out value="${borrow.game.gameName}"/> 
                <b>Lener: </b><c:out value="${borrow.borrower.borrowerName}"/>
                <b>Uitleendatum: </b><c:out value="${borrow.borrowDate}"/> 
                <b>Retourdatum: </b><c:out value="${borrow.returnDate}"/>
            </p>
        </c:forEach>
            
            <script>
                document.getElementById('filter').onchange = function(){
                    let id = this.options[this.selectedIndex].value;
                    
                    if(id != -1){
                        window.location = "?borrows&filter=" + id;
                    } else {
                        window.location = "?borrows";
                    }
                    
                }
            </script>
    </body>
</html>
