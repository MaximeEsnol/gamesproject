<%-- 
    Document   : gamedetails.jsp
    Created on : Feb 8, 2020, 6:47:05 PM
    Author     : MEOBL79
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Speldetails</title>
    </head>
    <body>
        <h1><c:out value="${game.gameName}"/></h1>
        <p><c:out value="${game.toString()}"/></p>
        <p>Category: <c:out value="${game.category.categoryName}"/></p>
        <p>Difficulty: <c:out value="${game.difficulty.name}"/></p>
    </body>
</html>
