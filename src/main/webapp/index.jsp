<%-- 
    Document   : index.jsp
    Created on : Feb 5, 2020, 12:04:03 PM
    Author     : MEOBL79
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Games Find</title>
    </head>
    <body>
        <h1>Spellen vinden - Maxime Esnol</h1>
        
        <p><a href="search?id=1">
            <button>Toon de eerste spelsoort</button>
            </a></p>
        
        <p><a href="search?game=5">
            <button>Toon het vijfde spel</button>
        </a></p>
        
        <p><a href="search?borrower=1">
                <button>Toon de eerste lener</button>
        </a></p>
        
        <form action="search" method="get">
            <input type="text" name="search" id="search"/>
            <button type="submit" disabled id="searchButton" name="searchButton">Search!</button>
        </form>
        
        <p>
            <a href="search?allgames">
                <button>Toon alle spellen</button>
            </a>
        </p>
        
        <p>
            <a href="search?borrows">
                <button>Toon uitleningen</button>
            </a>
        </p>
        
        <p>
            <a href="advanced">
                Uitgebreid zoeken
            </a>
        </p>
        <script>
            document.getElementById('search').onkeyup = function() {
                if(this.value.length > 0){
                    document.getElementById('searchButton').removeAttribute('disabled');
                } else {
                    document.getElementById('searchButton').setAttribute('disabled', 'disabled');
                }
            }
        </script>
    </body>
</html>
